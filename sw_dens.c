# include "seawater.h"

double
sw_dens (double S, double T, double P)
{
// ported from Matlab to C -- John H. Dunlap, APL-UW, May 12, 2005

// SW_DENS    Density of sea water
//=========================================================================
// SW_DENS  $Revision: 1.3 $  $Date: 1994/10/10 04:39:16 $
//          Copyright (C) CSIRO, Phil Morgan 1992.
//
// USAGE:  dens = sw_dens(S,T,P)
//
// DESCRIPTION:
//    Density of Sea Water using UNESCO 1983 (EOS 80) polynomial.
//
// INPUT:  (all must have same dimensions)
//   S = salinity    [psu      (PSS-78)]
//   T = temperature [degree C (ITS-90)]
//   P = pressure    [db]
//
// OUTPUT:
//   dens = density  [kg/m^3] 
// 
// AUTHOR:  Phil Morgan 92-11-05  (morgan@ml.csiro.au)
//
// DISCLAIMER:
//   This software is provided "as is" without warranty of any kind.  
//   See the file sw_copy.m for conditions of use and licence.
//
// REFERENCES:
//    Fofonoff, P. and Millard, R.C. Jr
//    Unesco 1983. Algorithms for computation of fundamental properties of 
//    seawater, 1983. _Unesco Tech. Pap. in Mar. Sci._, No. 44, 53 pp.
//
//    Millero, F.J., Chen, C.T., Bradshaw, A., and Schleicher, K.
//    " A new high pressure equation of state for seawater"
//    Deap-Sea Research., 1980, Vol27A, pp255-264.
//=========================================================================

// CALLER: general purpose
// CALLEE: sw_dens0.m sw_seck.m

// UNESCO 1983. eqn.7  p.15

//------
// BEGIN
//------
  double densP0, K, dens;

  densP0 = sw_dens0 (S, T);
  K = sw_seck (S, T, P);
  dens = densP0 / (1 - 0.1 * P / K);    // convert P from db to atm pressure units

  return dens;
//--------------------------------------------------------------------
}
