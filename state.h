#ifndef _STATE_H_
#define _STATE_H_

#include <sys/time.h>
#include "parser.h"

typedef struct {
    double piston_want;
    double piston_now;
    double p0, pnow;
    int piston_dir;
    int got_pnow;
    int got_piston;
    int valve_closed;
    struct timeval t_piston;
} SysState_t;

void state_init(SysState_t *state, double pr0);
double state_dt(SysState_t *state, struct timeval *tref);
double state_adv_piston(SysState_t *state, struct timeval *tref);
ParserState state_next(SysState_t *state, LogParser_t *p, int c);

#endif
