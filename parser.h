#ifndef _PARSER_H_
#define _PARSER_H_

#include <errno.h>
#include <stdlib.h>

typedef struct {
    char        *buf;
    size_t      bufsize;
    size_t      ibuf;
} CharBuf_t;

int cb_init(CharBuf_t *p, size_t init_size);
void cb_free(CharBuf_t *p);
int cb_add_char(CharBuf_t *p, int c);
void cb_reset(CharBuf_t *p);

typedef enum {
    INVALID=-1,
    START=0,
    RD_TIMESTAMP,
    RD_CLOCK,
    SKIP_SPACES,
    RD_FUNCNAME,
    RD_TEXT,
    GOT_TOKEN,
    DONE,
    PARSE_ERROR,
} ParserState;

#define STATUS_OK       0

typedef struct {
    ParserState state, hold;
    CharBuf_t   buf;
    CharBuf_t   token;
    CharBuf_t   func_name;
} LogParser_t;

int parser_init(LogParser_t *p, size_t n);
ParserState parser_add_char(LogParser_t *p, int c);
void parser_reset(LogParser_t *p);
const char *parser_funcname(LogParser_t *p);
const char *parser_token(LogParser_t *p);
const char *parser_line(LogParser_t *p);

#endif /* _PARSER_H_ */
