# include <math.h>
# include "seawater.h"

double
sw_ptmp (double S, double T, double P, double PR)
{
// ported from Matlab to C -- John H. Dunlap, APL-UW, May 12, 2005

// SW_PTMP    Potential temperature
//===========================================================================
// SW_PTMP  $Revision: 1.3 $  $Date: 1994/10/10 05:45:13 $
//          Copyright (C) CSIRO, Phil Morgan 1992. 
//
// USAGE:  ptmp = sw_ptmp(S, T, P, PR) 
//
// DESCRIPTION:
//    Calculates potential temperature as per UNESCO 1983 report.
//   
// INPUT:  (all must have same dimensions)
//   S  = salinity    [psu      (PSS-78) ]
//   T  = temperature [degree C (ITS-90)]
//   P  = pressure    [db]
//   PR = Reference pressure  [db]
//
// OUTPUT:
//   ptmp = Potential temperature relative to PR [degree C (ITS-90)]
//
// AUTHOR:  Phil Morgan 92-04-06  (morgan@ml.csiro.au)
//
// DISCLAIMER:
//   This software is provided "as is" without warranty of any kind.  
//   See the file sw_copy.m for conditions of use and licence.
//
// REFERENCES:
//    Fofonoff, P. and Millard, R.C. Jr
//    Unesco 1983. Algorithms for computation of fundamental properties of 
//    seawater, 1983. _Unesco Tech. Pap. in Mar. Sci._, No. 44, 53 pp.
//    Eqn.(31) p.39
//
//    Bryden, H. 1973.
//    "New Polynomials for thermal expansion, adiabatic temperature gradient
//    and potential temperature of sea water."
//    DEEP-SEA RES., 1973, Vol20,401-408.
//=========================================================================

// CALLER:  general purpose
// CALLEE:  sw_adtg.m

//------
// BEGIN
//------

  double del_P;
  double del_th;
  double th;
  double q;
  double sqrt2 = sqrt (2.0);
  double inv_sqrt2 = 1 / sqrt2;
  double PT;

// theta1
  del_P = PR - P;
  del_th = del_P * sw_adtg (S, T, P);
  th = T + 0.5 * del_th;
  q = del_th;

// theta2
  del_th = del_P * sw_adtg (S, th, P + 0.5 * del_P);
  th = th + (1 - inv_sqrt2) * (del_th - q);
  q = (2 - sqrt2) * del_th + (-2 + 3 * inv_sqrt2) * q;

// theta3
  del_th = del_P * sw_adtg (S, th, P + 0.5 * del_P);
  th = th + (1 + inv_sqrt2) * (del_th - q);
  q = (2 + sqrt2) * del_th + (-2 - 3 * inv_sqrt2) * q;

// theta4
  del_th = del_P * sw_adtg (S, th, P + del_P);
  PT = th + (del_th - 2 * q) / 6;

  return PT;
//=========================================================================
}
