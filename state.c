// Functions to track the state of the float.
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "state.h"
#include "parser.h"

#define COUNTS_PER_SECOND (double)0.2

static int starts_with(const char *s, const char *prefix);

static int starts_with(const char *s, const char *prefix)
{
    return strstr(s, prefix) == s;
}

// Initialize the SysState structure and set the minimum pressure
void state_init(SysState_t *state, double pr0)
{
    memset((void*)state, 0, sizeof(SysState_t));
    state->p0 = pr0;
}

// Return the time difference in seconds between tref and the time
// of the last piston position update.
double state_dt(SysState_t *state, struct timeval *tref)
{
    struct timeval *t0 = &(state->t_piston);
    return (tref->tv_sec - t0->tv_sec) + (tref->tv_usec - t0->tv_usec)*1.0e-6;
}

// Advance piston position to the supplied reference time
double state_adv_piston(SysState_t *state, struct timeval *tref)
{
    double dt = state_dt(state, tref);
    double piston = state->piston_now + dt * state->piston_dir * COUNTS_PER_SECOND;

    switch(state->piston_dir)
    {
        case 1:
            if(piston > state->piston_want)
                piston = state->piston_want;
            break;
        case -1:
            if(piston < state->piston_want)
                piston = state->piston_want;
            break;
    }

    return piston;
}


// Update state with the next character from the input.
ParserState state_next(SysState_t *state, LogParser_t *p, int c)
{
    ParserState ps;
    char        *cp;
    char        *endp = 0;
    int         n, iN, iW;

    ps = parser_add_char(p, c);
    switch(ps)
    {
        case GOT_TOKEN:
            // Parse the new piston positions in real-time
            if(strcmp(parser_funcname(p), "PistonMoveAbsWTO()") == 0)
            {
                if((cp = strstr(parser_token(p), "->")) != NULL)
                {
                    n = sscanf(parser_token(p), "%03d->%03d", &iN, &iW);
                    if(n == 2)
                    {
                        state->piston_now = iN;
                        state->piston_want = iW;
                        state->got_piston = 1;
                        if (iN < iW)      state->piston_dir = +1;
                        else if (iN > iW) state->piston_dir = -1;
                        else              state->piston_dir = 0;
                        gettimeofday(&(state->t_piston), NULL);
                    }
                }
                else
                {
                    endp = 0;
                    iN = strtol(parser_token(p), &endp, 10);
                    if(*endp == 0)
                    {
                        // Token is a single integer
                        state->piston_now = iN;
                        state->got_piston = 1;
                        gettimeofday(&(state->t_piston), NULL);
                    }
                }
                return ps;
            }


            break;
        case DONE:
            // All other state changes can wait until the end-of-line
            // character is received.
            if(state->got_pnow == 0 &&
               strcmp(parser_funcname(p), "Descent()") == 0)
            {
                double pval;

                // The final token is the pressure value
                endp = 0;
                pval = strtod(parser_token(p), &endp);
                if(*endp == 0)
                {
                    state->pnow = pval;
                    state->got_pnow = 1;
                }
                return ps;
            }

            if(strcmp(parser_funcname(p), "AirValveOpen()") == 0)
            {
                state->valve_closed = 0;
                return ps;
            }

            if(strcmp(parser_funcname(p), "AirValveClose()") == 0)
            {
                // Set current pressure to the top of the profile.
                // The float will stay at the top of the profile
                // until the air-valve is opened.
                state->valve_closed = 1;
                state->pnow = state->p0;
                state->got_pnow = 1;
                return ps;
            }

            if(starts_with(parser_line(p), "Mission activated"))
            {
                // Set current pressure to the top of the profile
                // and mark the air-valve as open.
                state->pnow = state->p0;
                state->got_pnow = 1;
                state->valve_closed = 0;
                return ps;
            }

            if(starts_with(parser_line(p), "Attempting to start mission"))
            {
                state->pnow = state->p0;
                state->got_pnow = 1;

                // Prevent sinking while moving piston
                state->piston_now = 227;
                state->piston_dir = 0;
                state->got_piston = 1;
                return ps;
            }
            break;
        default:
            break;
    }

    return ps;
}
