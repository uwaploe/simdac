// Pressure sensor calibrations
#include <stdio.h>
#include "cals.h"

static PrCal_t cals[] = {
    { .serno = 8321, .offset = 32.5, .mult = 20.3141 }, // SBE41PLUS
    { .serno = 8106, .offset = 1.4, .mult = 21.0393 }, // SBE41PLUS from EM-APEX 7700 or 7701
    { .serno = 2998, .offset = 10.5, .mult = 20.3141 }, // SBE41CP from EM-APEX 4973
    { .serno = 2681, .offset = 0.75, .mult = 40.0 }, // SBE41 from 2240 (Victoria)
    { .serno = 13073, .offset = 10.22, .mult = 15.1515} // SBE41, APF-11
};

PrCal_t *lookup_cal(int sernum)
{
    // Linear search is fine for such a small number of elements
    int n = sizeof(cals)/sizeof(PrCal_t);
    for(int i = 0; i < n; i++)
    {
        if(sernum == cals[i].serno)
            return &cals[i];
    }

    return NULL;
}
