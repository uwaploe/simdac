#ifndef _CALS_H_
#define _CALS_H_

typedef struct {
    int serno;
    float offset;
    float mult;
} PrCal_t;

PrCal_t *lookup_cal(int sernum);

#endif // _CALS_H_
