VERSION ?= $(shell git describe --tags --always --dirty --match=v* 2> /dev/null || echo v0)
GIT_HASH ?= $(shell git log -1 --format=%H)
GIT_REV ?= $(shell git log -1 --format=%h)
# Get the file in the branch referencing the commit SHA
git_current := $(shell cut -c6- .git/HEAD)

CFLAGS=-Wall

OBJS := simdac.o \
        cals.o \
	state.o \
	parser.o

SW_OBJS := sw_adtg.o \
	   sw_dens0.o \
           sw_dens.o \
           sw_pden.o \
           sw_ptmp.o \
           sw_seck.o \
           sw_smow.o

all: simdac

simdac.o: version.h

simdac: $(OBJS) $(SW_OBJS)
	$(CC) $(OBJS) $(SW_OBJS) -lm -o simdac

version.h: version.h.in .git/$(git_current)
	@echo "Generating version.h"
	@sed -e "s!@VERSION@!$(VERSION)!g" \
	-e "s!@GIT_REV@!$(GIT_REV)!g" \
	-e "s!@GIT_HASH@!$(GIT_HASH)!g" version.h.in > version.h

.PHONY: clean tests

clean:
	rm -f $(OBJS) $(SW_OBJS) simdac version.h

tests:
	cd testing && $(MAKE) tests
