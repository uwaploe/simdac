# include "seawater.h"

double
sw_smow (double T)
{
// converted from Matlab to C -- John H. Dunlap, APL-UW, May 12, 2005

// SW_SMOW    Density of standard mean ocean water (pure water)
//=========================================================================
// SW_SMOW  $Revision: 1.3 $  $Date: 1994/10/10 05:51:46 $
//          Copyright (C) CSIRO, Phil Morgan 1992.
//
// USAGE:  dens = sw_smow(T)
//
// DESCRIPTION:
//    Density of Standard Mean Ocean Water (Pure Water) using EOS 1980. 
//
// INPUT: 
//   T = temperature [degree C (ITS-90)]
//
// OUTPUT:
//   dens = density  [kg/m^3] 
// 
// AUTHOR:  Phil Morgan 92-11-05  (morgan@ml.csiro.au)
//
// DISCLAIMER:
//   This software is provided "as is" without warranty of any kind.  
//   See the file sw_copy.m for conditions of use and licence.
//
// REFERENCES:
//     Unesco 1983. Algorithms for computation of fundamental properties of 
//     seawater, 1983. _Unesco Tech. Pap. in Mar. Sci._, No. 44, 53 pp.
//     UNESCO 1983 p17  Eqn(14)
//
//     Millero, F.J & Poisson, A.
//     INternational one-atmosphere equation of state for seawater.
//     Deep-Sea Research Vol28A No.6. 1981 625-629.    Eqn (6)
//=========================================================================

//----------------------
// DEFINE CONSTANTS
//----------------------
  double a0 = 999.842594;
  double a1 = 6.793952e-2;
  double a2 = -9.095290e-3;
  double a3 = 1.001685e-4;
  double a4 = -1.120083e-6;
  double a5 = 6.536332e-9;
  double dens;

  T *= 1.00024;                 // convert from T90 to T68

  dens = a0 + (a1 + (a2 + (a3 + (a4 + a5 * T) * T) * T) * T) * T;

  return dens;
//--------------------------------------------------------------------
}
