// sigmatheta.c -- check that computation is that desired

# include <stdio.h>
# include <stdlib.h>

# include "seawater.h"

int
main ()
{

  double Plist[] = { 0, 50, 100, 150 };
  double Tlist[] = { 25, 15, 5 };
  double Slist[] = { 35, 33, 31 };
  double Pref = 0;
  int iP, iT, iS;
  double P, T, S, SigTh, Rho;

  fprintf (stdout, "Pref=%6.1f\n", Pref);

  for (iP = 0; iP < sizeof (Plist) / sizeof (*Plist); iP++)
  {
    for (iT = 0; iT < sizeof (Tlist) / sizeof (*Tlist); iT++)
    {
      printf ("\n");
      for (iS = 0; iS < sizeof (Slist) / sizeof (*Slist); iS++)
      {

        P = Plist[iP];
        T = Tlist[iT];
        S = Slist[iS];

        SigTh = sw_pden (S, T, P, Pref) - 1000;
        Rho = sw_dens (S, T, P);

        printf ("P=%6.1f T=%7.3f S=%7.3f SigTh=%7.3f Rho-1000=%7.3f\n",
                P, T, S, SigTh, Rho - 1000);

      }                         // for iS
    }                           // for iT
  }                             // for iP

  exit (0);

}
