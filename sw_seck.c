# include <math.h>
# include "seawater.h"

double
sw_seck (double S, double T, double P)
{
// converted from Matlab to C -- John H. Dunlap, APL-UW, May 12, 2005

// SW_SECK    Secant bulk modulus (K) of sea water
//=========================================================================
// SW_SECK  $Revision: 1.3 $  $Date: 1994/10/10 05:50:45 $
//          Copyright (C) CSIRO, Phil Morgan 1992.
//
// USAGE:  dens = sw_seck(S,T,P)
//
// DESCRIPTION:
//    Secant Bulk Modulus (K) of Sea Water using Equation of state 1980.
//    UNESCO polynomial implementation.
//
// INPUT:  (all must have same dimensions)
//   S = salinity    [psu      (PSS-78) ]
//   T = temperature [degree C (ITS-90)]
//   P = pressure    [db]
//
// OUTPUT:
//   K = Secant Bulk Modulus  [bars]
//
// AUTHOR:  Phil Morgan 92-11-05  (morgan@ml.csiro.au)
//
// DISCLAIMER:
//   This software is provided "as is" without warranty of any kind.
//   See the file sw_copy.m for conditions of use and licence.
//
// REFERENCES:
//    Fofonoff, P. and Millard, R.C. Jr
//    Unesco 1983. Algorithms for computation of fundamental properties of
//    seawater, 1983. _Unesco Tech. Pap. in Mar. Sci._, No. 44, 53 pp.
//    Eqn.(15) p.18
//
//    Millero, F.J. and  Poisson, A.
//    International one-atmosphere equation of state of seawater.
//    Deep-Sea Res. 1981. Vol28A(6) pp625-629.
//=========================================================================

// CALLER: sw_dens.m
// CALLEE: none

  double h3 = -5.77905E-7;
  double h2 = +1.16092E-4;
  double h1 = +1.43713E-3;
  double h0 = +3.239908;        //[-0.1194975];

  double k2 = 5.2787E-8;
  double k1 = -6.12293E-6;
  double k0 = +8.50935E-5;      //[+3.47718E-5];

  double e4 = -5.155288E-5;
  double e3 = +1.360477E-2;
  double e2 = -2.327105;
  double e1 = +148.4206;
  double e0 = 19652.21;         //[-1930.06];

  double j0 = 1.91075E-4;

  double i2 = -1.6078E-6;
  double i1 = -1.0981E-5;
  double i0 = 2.2838E-3;

  double m2 = 9.1697E-10;
  double m1 = +2.0816E-8;
  double m0 = -9.9348E-7;

  double f3 = -6.1670E-5;
  double f2 = +1.09987E-2;
  double f1 = -0.603459;
  double f0 = +54.6746;

  double g2 = -5.3009E-4;
  double g1 = +1.6483E-2;
  double g0 = +7.944E-2;

  double AW, BW, KW;
  double SR;
  double A, B, K0, K;

//--------------------------------------------------------------------
// COMPUTE COMPRESSION TERMS
//--------------------------------------------------------------------
  P *= 0.1;                     //convert from dbar to bar, atmospheric pressure units

  T *= 1.00024;                 // convert from T90 to T68

// Pure water terms of the secant bulk modulus at atmos pressure.
// UNESCO eqn 19 p 18

  AW = h0 + (h1 + (h2 + h3 * T) * T) * T;

  BW = k0 + (k1 + k2 * T) * T;

  KW = e0 + (e1 + (e2 + (e3 + e4 * T) * T) * T) * T;    // eqn 19

//--------------------------------------------------------------------
// SEA WATER TERMS OF SECANT BULK MODULUS AT ATMOS PRESSURE.
//--------------------------------------------------------------------
  SR = sqrt (S);

  A = AW + (i0 + (i1 + i2 * T) * T + j0 * SR) * S;

  B = BW + (m0 + (m1 + m2 * T) * T) * S;        // eqn 18

  K0 = KW + (f0 + (f1 + (f2 + f3 * T) * T) * T + (g0 + (g1 + g2 * T) * T) * SR) * S;    // eqn 16

  K = K0 + (A + B * P) * P;     // eqn 15

  return K;
//----------------------------------------------------------------------------
}
