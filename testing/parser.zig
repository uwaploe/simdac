// Use Zig to test the parser functions
const clib = @cImport({
    @cInclude("parser.h");
});
const std = @import("std");
const testing = std.testing;

fn newCharBuf_t() clib.CharBuf_t {
    return clib.CharBuf_t{
        .buf = 0,
        .bufsize = 0,
        .ibuf = 0,
    };
}

fn newLogParser_t() clib.LogParser_t {
    return clib.LogParser_t{
        .state = -1,
        .hold = -1,
        .buf = newCharBuf_t(),
        .token = newCharBuf_t(),
        .func_name = newCharBuf_t(),
    };
}

fn cvtCString(str: [*c]const u8) [:0]const u8 {
    return std.mem.span(str);
}

test "init" {
    var p = newLogParser_t();
    var status = clib.parser_init(&p, 512);
    try testing.expect(p.state == clib.START);
    try testing.expect(status == 0);
}

test "function name" {
    const buf = "(Dec 15 2018 04:44:13,  148207 sec) PistonMoveAbsWTO()    016->038 017 018 019 020 021 [30sec, 14.6Volts, 0.744Amps, CPT:1006sec]\n";
    var p = newLogParser_t();
    _ = clib.parser_init(&p, 512);
    const ok: bool = for (buf) |char| {
        var state = clib.parser_add_char(&p, @intCast(char));
        if (state == clib.RD_TEXT) {
            // Need to reach this state before the function
            // name is available.
            break true;
        }
    } else false;
    try testing.expect(ok);
    try testing.expectEqualStrings("PistonMoveAbsWTO()", std.mem.span(clib.parser_funcname(&p)));
}

test "standard" {
    const buf = "(Oct 27 2006 03:22:35,     629 sec) Descent()            Pressure: 46.1\n";
    var p = newLogParser_t();
    _ = clib.parser_init(&p, 512);
    var n_token: u8 = 0;
    for (buf) |char| {
        var state = clib.parser_add_char(&p, @intCast(char));
        switch (state) {
            clib.GOT_TOKEN => {
                n_token += 1;
            },
            clib.DONE => {
                break;
            },
            else => {
                try testing.expect(state != clib.PARSE_ERROR);
            },
        }
    }
    try testing.expectEqualStrings("46.1", std.mem.span(clib.parser_token(&p)));
    try testing.expectFmt("1", "{}", .{n_token});
}

test "no text" {
    const buf = "(Jan 01 1970 00:38:27,     290 sec) AirValveOpen()\n";
    var p = newLogParser_t();
    _ = clib.parser_init(&p, 512);
    var n_token: u8 = 0;
    for (buf) |char| {
        var state = clib.parser_add_char(&p, @intCast(char));
        switch (state) {
            clib.GOT_TOKEN => {
                n_token += 1;
            },
            clib.DONE => {
                break;
            },
            else => {
                try testing.expect(state != clib.PARSE_ERROR);
            },
        }
    }
    try testing.expectFmt("0", "{}", .{n_token});
    try testing.expectEqualStrings(buf, std.mem.span(clib.parser_line(&p)));
}

test "trailing space" {
    const buf = "(Jan 01 1970 00:38:27,     290 sec) AirValveOpen()  \n";
    var p = newLogParser_t();
    _ = clib.parser_init(&p, 512);
    var n_token: u8 = 0;
    for (buf) |char| {
        var state = clib.parser_add_char(&p, @intCast(char));
        switch (state) {
            clib.GOT_TOKEN => {
                n_token += 1;
            },
            clib.DONE => {
                break;
            },
            else => {
                try testing.expect(state != clib.PARSE_ERROR);
            },
        }
    }
    try testing.expectFmt("0", "{}", .{n_token});
    try testing.expectEqualStrings(buf, std.mem.span(clib.parser_line(&p)));
}

test "no timestamp" {
    const buf = "Descent()            Pressure: 1008.0\n";
    var p = newLogParser_t();
    _ = clib.parser_init(&p, 512);
    var n_token: u8 = 0;
    for (buf) |char| {
        var state = clib.parser_add_char(&p, @intCast(char));
        switch (state) {
            clib.GOT_TOKEN => {
                n_token += 1;
            },
            clib.DONE => {
                break;
            },
            else => {
                try testing.expect(state != clib.PARSE_ERROR);
            },
        }
    }
    try testing.expectFmt("0", "{}", .{n_token});
    try testing.expectEqualStrings(buf, std.mem.span(clib.parser_line(&p)));
}

test "piston steps" {
    const buf = "(Dec 15 2018 04:44:13,  148207 sec) PistonMoveAbsWTO()    016->038 017 018 019 020 021 [30sec, 14.6Volts, 0.744Amps, CPT:1006sec]\n";
    var p = newLogParser_t();
    _ = clib.parser_init(&p, 512);
    var n_token: u8 = 0;
    for (buf) |char| {
        var state = clib.parser_add_char(&p, @intCast(char));
        switch (state) {
            clib.GOT_TOKEN => {
                n_token += 1;
                switch (n_token) {
                    1 => {
                        try testing.expectEqualStrings("016->038", std.mem.span(clib.parser_token(&p)));
                    },
                    2 => {
                        try testing.expectEqualStrings("017", std.mem.span(clib.parser_token(&p)));
                    },
                    3 => {
                        try testing.expectEqualStrings("018", std.mem.span(clib.parser_token(&p)));
                    },
                    4 => {
                        try testing.expectEqualStrings("019", std.mem.span(clib.parser_token(&p)));
                    },
                    5 => {
                        try testing.expectEqualStrings("020", std.mem.span(clib.parser_token(&p)));
                    },
                    6 => {
                        try testing.expectEqualStrings("021", std.mem.span(clib.parser_token(&p)));
                    },
                    7 => {
                        try testing.expectEqualStrings("[30sec,", std.mem.span(clib.parser_token(&p)));
                    },
                    else => {},
                }
            },
            clib.DONE => {
                // Confirm final token
                try testing.expectEqualStrings("CPT:1006sec]", std.mem.span(clib.parser_token(&p)));
                break;
            },
            else => {
                try testing.expect(state != clib.PARSE_ERROR);
            },
        }
    }
    try testing.expectFmt("9", "{}", .{n_token});
}

// Local Variables:
// compile-command: "zig build test"
// End:
