const std = @import("std");

// Although this function looks imperative, note that its job is to
// declaratively construct a build graph that will be executed by an external
// runner.
pub fn build(b: *std.Build) void {
    // Standard target options allows the person running `zig build` to choose
    // what target to build for. Here we do not override the defaults, which
    // means any target is allowed, and the default is native. Other options
    // for restricting supported target set are available.
    const target = b.standardTargetOptions(.{});

    // Standard optimization options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall. Here we do not
    // set a preferred release mode, allowing the user to decide how to optimize.
    const optimize = b.standardOptimizeOption(.{});

    // Creates a step for unit testing.
    const parser_tests = b.addTest(.{
        .root_source_file = .{ .path = "parser.zig" },
        .target = target,
        .optimize = optimize,
    });
    parser_tests.addIncludePath(.{ .path = "../" });
    parser_tests.addCSourceFile(.{ .file = .{ .path = "../parser.c" }, .flags = &[_][]const u8{
        "-std=gnu99",
    } });

    // Link the C library
    parser_tests.linkLibC();

    const state_tests = b.addTest(.{
        .root_source_file = .{ .path = "state.zig" },
        .target = target,
        .optimize = optimize,
    });
    state_tests.addIncludePath(.{ .path = "../" });
    state_tests.addCSourceFile(.{ .file = .{ .path = "../state.c" }, .flags = &[_][]const u8{
        "-std=gnu99",
    } });
    state_tests.addCSourceFile(.{ .file = .{ .path = "../parser.c" }, .flags = &[_][]const u8{
        "-std=gnu99",
    } });

    // Link the C library
    state_tests.linkLibC();

    // This creates a build step. It will be visible in the `zig build --help` menu,
    // and can be selected like this: `zig build test`
    // This will evaluate the `test` step rather than the default, which is "install".
    const test_step = b.step("test", "Run library tests");
    test_step.dependOn(&parser_tests.step);
    test_step.dependOn(&state_tests.step);
}
