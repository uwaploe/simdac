// Use Zig to test the state update functionality.
const clib = @cImport({
    @cInclude("state.h");
});
const std = @import("std");
const testing = std.testing;

// Zig requires that all struct fields have a value
fn newSysState_t() clib.SysState_t {
    return clib.SysState_t{
        .piston_want = 0.0,
        .piston_now = 0.0,
        .p0 = 0.0,
        .pnow = 0.0,
        .piston_dir = 0,
        .got_pnow = 0,
        .got_piston = 0,
        .valve_closed = 0,
        .t_piston = clib.timeval{ .tv_sec = 0, .tv_usec = 0 },
    };
}

fn newCharBuf_t() clib.CharBuf_t {
    return clib.CharBuf_t{
        .buf = 0,
        .bufsize = 0,
        .ibuf = 0,
    };
}

fn newLogParser_t() clib.LogParser_t {
    return clib.LogParser_t{
        .state = -1,
        .hold = -1,
        .buf = newCharBuf_t(),
        .token = newCharBuf_t(),
        .func_name = newCharBuf_t(),
    };
}

test "state init" {
    var s = newSysState_t();
    clib.state_init(&s, 42.0);
    try testing.expect(s.got_piston == 0);
    try testing.expect(s.got_pnow == 0);
    try testing.expect(s.p0 == 42.0);
}

test "pressure init" {
    const buf = "(Oct 27 2006 03:22:35,     629 sec) Descent()            Pressure: 46.1\n";
    var s = newSysState_t();
    var p = newLogParser_t();
    _ = clib.parser_init(&p, 512);
    clib.state_init(&s, 0.0);
    s.pnow = 0.1;
    s.got_pnow = 1;
    for (buf) |char| {
        var state = clib.state_next(&s, &p, @intCast(char));
        switch (state) {
            clib.DONE => {
                break;
            },
            else => {},
        }
    }

    // Pressure provided by the Descent should be ignored
    // because the starting pressure value was already set.
    try testing.expectFmt("1", "{d}", .{s.got_pnow});
    try testing.expect(s.pnow == 0.1);
}

test "read pressure" {
    const buf = "(Oct 27 2006 03:22:35,     629 sec) Descent()            Pressure: 46.1\r\n";
    var s = newSysState_t();
    var p = newLogParser_t();
    _ = clib.parser_init(&p, 512);
    clib.state_init(&s, 0.0);
    s.got_pnow = 0;
    for (buf) |char| {
        var state = clib.state_next(&s, &p, @intCast(char));
        switch (state) {
            clib.DONE => {
                break;
            },
            else => {},
        }
    }

    try testing.expectFmt("46.1", "{d}", .{s.pnow});
    try testing.expectFmt("1", "{d}", .{s.got_pnow});
}

test "valve open" {
    const buf = "(Jan 01 1970 00:30:00,     298 sec) AirValveOpen()\r\n";
    var s = newSysState_t();
    var p = newLogParser_t();
    _ = clib.parser_init(&p, 512);
    clib.state_init(&s, 0.0);
    s.valve_closed = 1;
    for (buf) |char| {
        var state = clib.state_next(&s, &p, @intCast(char));
        switch (state) {
            clib.DONE => {
                break;
            },
            else => {},
        }
    }

    try testing.expect(s.valve_closed == 0);
}

test "valve close" {
    const buf = "(Jan 01 1970 00:30:02,     300 sec) AirValveClose() \r\n";
    var s = newSysState_t();
    var p = newLogParser_t();
    _ = clib.parser_init(&p, 512);
    clib.state_init(&s, 42.0);
    for (buf) |char| {
        var state = clib.state_next(&s, &p, @intCast(char));
        switch (state) {
            clib.DONE => {
                break;
            },
            else => {},
        }
    }

    try testing.expectFmt("42", "{d}", .{s.pnow});
    try testing.expect(s.valve_closed == 1);
    try testing.expect(s.got_pnow == 1);
}

test "piston steps" {
    const buf = "(Dec 15 2018 04:44:13,  148207 sec) PistonMoveAbsWTO()    016->038 017 018 019 020 021 [30sec, 14.6Volts, 0.744Amps, CPT:1006sec]\n";
    var p = newLogParser_t();
    var s = newSysState_t();
    _ = clib.parser_init(&p, 512);
    clib.state_init(&s, 0.0);
    var n_token: u8 = 0;
    for (buf) |char| {
        var state = clib.state_next(&s, &p, @intCast(char));
        switch (state) {
            clib.GOT_TOKEN => {
                n_token += 1;
                switch (n_token) {
                    1 => {
                        try testing.expectEqualStrings("016->038", std.mem.span(clib.parser_token(&p)));
                    },
                    2 => {
                        try testing.expectFmt("17", "{d}", .{s.piston_now});
                    },
                    3 => {
                        try testing.expectFmt("18", "{d}", .{s.piston_now});
                    },
                    4 => {
                        try testing.expectFmt("19", "{d}", .{s.piston_now});
                    },
                    5 => {
                        try testing.expectFmt("20", "{d}", .{s.piston_now});
                    },
                    6 => {
                        try testing.expectFmt("21", "{d}", .{s.piston_now});
                    },
                    7 => {
                        try testing.expectFmt("21", "{d}", .{s.piston_now});
                    },
                    else => {},
                }
            },
            clib.DONE => {
                try testing.expectFmt("21", "{d}", .{s.piston_now});
                break;
            },
            else => {
                try testing.expect(state != clib.PARSE_ERROR);
            },
        }
    }
    try testing.expectFmt("9", "{}", .{n_token});
}

// Local Variables:
// compile-command: "zig build test"
// End:
