# Unit testing using Zig

This directory contains unit tests for the `simdac` components. The tests are written using a new programming language called [Zig](https://ziglang.org). Zig has built-in support for unit testing, the ability to [call C functions](https://ziglang.org/learn/overview/#integration-with-c-libraries-without-ffibindings), and the ability to [compile C source code](https://ziglang.org/learn/overview/#zig-is-also-a-c-compiler).

## Usage

Visit the first URL above for instructions on how to install Zig, then simply run the command `make tests` in this directory or the parent directory.
