// simdac.c -- simulate pressure signal using DAC for SBE 41 or 41cp
// integrates pressure from computed dpdt from piston position
// reads EM-APEX console port, writes to serially controlled
// digital to analog converter which replaces the pressure
// transducer on the SBE-41cp.

// Dec 5, 2013 -- added code to read debugging output of GTD APEX
// 2022-2023 -- rewrite to support Apf-11
//
# include <stdio.h>
# include <stdlib.h>
# include <unistd.h>
# include <math.h>
# include <string.h>
# include <time.h>
# include <termios.h>            /* for settty() gettty() args */
# include <fcntl.h>             /* for open() args */
# include <ctype.h>
# include <sys/time.h>
# include <unistd.h>
# include <getopt.h>
# include <errno.h>
# include "seawater.h"
# include "cals.h"
# include "state.h"
# include "version.h"

#include <sys/types.h>

static int nprof;
# define MPROF 10000
static double Pprof[MPROF];
static double Tprof[MPROF];
static double Sprof[MPROF];

static double Pbp, Tbp, Sbp, PCbp, Rbp;
static double addwt = 0.0;

static double Tnow = 0.0;
static double Snow = 0.0;

static int debug = 1;

static char *ptsfile;

#include<signal.h>


double calc_pcn(double P,double T,double S);
int read_pts(void);
void sighup_handler(int signo);
int opentty (char *devtty, long baud);
double piston_neutral(SysState_t *state);

int read_pts(void)
{
  char *ptr;
  int nbal;
  int lineno;
  char tmp[100];
  char line[100];
  FILE *ptsfd;
  int iprof;
# define MTOK 6
  char *tok[MTOK];
  int ntok;

  fprintf(stderr,"ptsfile=%s\n",ptsfile);

  if ((ptsfd = fopen(ptsfile,"r")) == NULL)
  {
    fprintf (stderr, "error: cannot open PTS file=\"%s\"\n", ptsfile);
    exit(1);
  }

  lineno = 0;
  nprof = 0;
  nbal = 0;
  while (fgets(line, sizeof(line), ptsfd) != NULL)
  {
    lineno++;
    strcpy(tmp,line);

    if((ptr=strchr(tmp,'#')) != NULL)
    {
      *ptr = '\000';
    }

    ptr = tmp;
    for(ntok=0; ntok<MTOK; ntok++)
    {
      if((tok[ntok] = strtok(ptr, " ,\n")) == NULL)
        break;
      ptr = NULL;
    }

    if(ntok > 0)
    {
      if(strcmp(tok[0],"PTS") == 0)
      {
        if(ntok < 4)
        {
          fprintf (stderr, "error: PTS line should have at least four tokens; lineno=%d, line=[%s]\n", lineno,line);
          exit(1);
        }
        if(nprof < MPROF)
        {
          Pprof[nprof] = atof(tok[1]);
          Tprof[nprof] = atof(tok[2]);
          Sprof[nprof] = atof(tok[3]);
          nprof++;
        }
      }
      else if(strcmp(tok[0],"BAL") == 0)
      {
        if(ntok < 5 || ntok > 6)
        {
          fprintf (stderr, "error: BAL line should have five or six tokens. lineno=%d, line=[%s]\n", lineno,line);
          exit(1);
        }
        Pbp = atof(tok[1]);
        Tbp = atof(tok[2]);
        Sbp = atof(tok[3]);
        PCbp = atof(tok[4]);
        if(ntok == 6)
          addwt = atof(tok[5]);
        else
          addwt = 0.0;
        Rbp = sw_dens(Sbp,Tbp,Pbp) / 1000;
        nbal++;
      }
      else
      {
        fprintf(stderr,"unknown line prefix. lineno=%d, line=[%s]\n",lineno,line);
        exit(1);
      }
    }
  }
  fclose(ptsfd);

  if (nbal == 0)
  {
    fprintf (stderr, "error: no ballast point line found\n");
    exit(1);
  }

  if (nprof < 2)
  {
    fprintf (stderr, "error: PTS profile too short, nprof=%d\n", nprof);
    exit(1);
  }

  if(debug)
  {
    fprintf(stderr,"PTS profile:\n");
    for(iprof=0; iprof<nprof; iprof++)
    {
      fprintf(stderr,"%6.0f %6.3f %6.3f ",
        Pprof[iprof],Tprof[iprof],Sprof[iprof]);
      fprintf(stderr,"pcn= %3.0f\n",
        calc_pcn(Pprof[iprof],Tprof[iprof],Sprof[iprof]));

    }
    fprintf(stderr,"Ballast Point:\n");
    fprintf(stderr,"%6.0f %6.3f %6.3f pcbp=%g addwt=%g\n",
      Pbp,Tbp,Sbp,PCbp,addwt);
  }
  return 1;
}

void sighup_handler(int signo)
{
  if (signo == SIGHUP)
  {
    fprintf(stderr, "\nreceived SIGHUP -- calling read_pts()\n");
    read_pts();
  }
  else
  {
    fprintf(stderr, "\nreceived signal=%d -- calling exit(1)\n",signo);
    exit(1);
  }
}


// for serial ports
int opentty (char *devtty, long baud)
{
  static struct termios settty;
  int ret;
  int baudflag;
  int fd;

  if (strlen (devtty) == 1)
    return -1;

  // open first with no delay in order to change the bits to ignore
  // handshaking lines, then open again for real.

  if ((fd = open (devtty, O_RDWR | O_NDELAY)) < 0)
  {
    fprintf (stderr, "first open() of devtty failed\n");
    fprintf (stderr, "devtty=\"%s\"\n", devtty);
    return -1;
  }

  if (baud == 110) baudflag = B110;
  else if (baud == 300) baudflag = B300;
  else if (baud == 600) baudflag = B600;
  else if (baud == 1200) baudflag = B1200;
  else if (baud == 2400) baudflag = B2400;
  else if (baud == 4800) baudflag = B4800;
  else if (baud == 9600) baudflag = B9600;
  else if (baud == 19200) baudflag = B19200;
  else if (baud == 38400) baudflag = B38400;
  else if (baud == 57600) baudflag = B57600;
  else if (baud == 115200) baudflag = B115200;
  else if (baud == 230400) baudflag = B230400;
#ifdef B460800
  else if (baud == 460800) baudflag = B460800;
#endif
  else
  {
    fprintf (stderr, "opentty(): unknown baudrate=%ld\n", baud);
    return -1;
  }
  settty.c_iflag = 0;
  settty.c_oflag = 0;
  settty.c_cflag = 0;
  settty.c_lflag = 0;

  cfmakeraw (&settty);

  settty.c_cflag |= CLOCAL;   // ignore modem control lines
  settty.c_cflag &= ~CRTSCTS; // no flow control

  cfsetispeed (&settty, baudflag);
  cfsetospeed (&settty, baudflag);
  settty.c_cc[VMIN] = 1;
  settty.c_cc[VTIME] = 0;

  ret = tcsetattr (fd, TCSANOW, &settty);
  if (ret < 0)
  {
    fprintf (stderr, "tcsetattr() failed\n");
    return -1;
  }

  return fd;
}

// Return the piston count setting required to make the float
// neutrally buoyant at the current pressure, temperature, and
// salinity.
double calc_pcn(double P,double T,double S)
{
  double alpha = -3.8e-6;  // Compressibility factor
  double beta  =  3.2e-5;    // Thermal expansion factor
  double mass  = 27900.0;  // float mass in grams
  double pccpc  = 252.0 / (227.0-9.0); // piston cc/count
  double vbp;
  double vw;
  double vfp;
  double vft;
  double vf;
  double vpn;
  double pcn;
  double dens;

  dens   = sw_dens(S,T,P) / 1000.0; // in-site density in g/cc
  vbp = mass / Rbp;  // float volume at ballast point
  vw = mass / dens - vbp;
  vfp = mass * alpha * (P-Pbp);
  vft = mass * beta  * (T-Tbp);
  vf = vfp + vft;
  vpn = vw - vf;
  vpn = vpn + addwt;
  pcn = vpn / pccpc + PCbp;

  return pcn;
}

double piston_neutral(SysState_t *state)
{
  double frac;
  int i;

  for(i=1; i<nprof; i++)
    if(Pprof[i] > state->pnow)
      break;
  if(i>=nprof)
    i = nprof-1;

  frac = (state->pnow - Pprof[i-1]) / (Pprof[i] - Pprof[i-1]);
  Tnow = Tprof[i-1] + (Tprof[i] - Tprof[i-1]) * frac;
  Snow = Sprof[i-1] + (Sprof[i] - Sprof[i-1]) * frac;

  return calc_pcn(state->pnow, Tnow, Snow);
}

char * isonow(void)
{
  struct timeval tvnow;
  struct timezone tz;
  static char tmbuf[32];
  static char tmfmt[] = "%H:%M:%S";

  gettimeofday(&tvnow, &tz);
  strftime (tmbuf, sizeof(tmbuf), tmfmt, gmtime(&(tvnow.tv_sec)));
  return tmbuf;
}

// Scale pressure value and output on the DAC
int write_dac(int fd, double pr, PrCal_t *cal)
{
    int         dac, n;
    char        buf[20];

    dac = (pr + cal->offset) * cal->mult;
    if(dac < 0) dac = 0;
    if(dac > 0xFFFF) dac = 0xFFFF;

    n = snprintf(buf, sizeof(buf)-1, "$018%04X\r", dac);
    write(fd, buf, n);

    return dac;
}

static struct option longopts[] = {
    { "Pnow", required_argument, 0, 'p'},
    { "piston_now", required_argument, 0, 0},
    { "baud", required_argument, 0, 'b'},
    { "help", no_argument, 0, '?'},
    { "version", no_argument, 0, 'r'},
    { 0, 0, 0, 0}
};

static char *usage_msg[] = {
    "Usage: simdac [options] serno ptsfile ttyema ttydac\n",
    "\n",
    "Arguments:\n",
    "  serno    CTD serial number\n",
    "  ptsfile  name of pressure/temperature/salinity file\n",
    "  ttyema   TTY device connect to float console\n",
    "  ttydac   TTY device connected to DAC\n",
    "Options:\n",
    "  --help            show this message\n",
    "  --version         show program version and exit\n",
    "  --Pnow dbars      set starting pressure in decibars\n",
    "  --baud bps        set float console baud rate (default 9600)\n",
    "  --piston_now val  set starting piston value\n",
    NULL
};

void usage()
{
    char        **p;
    p = usage_msg;
    while(*p != NULL)
    {
        fputs(*p, stderr);
        p++;
    }
}

int
main (int argc, char **argv)
{
  long baud = 9600;
  long serno = 0;
  char *ttyema;
  char *ttydac;
  int fdema;
  int fddac;
  fd_set readfds;
  int nfds;
  int selret;
  PrCal_t *pr_cal;
  SysState_t sys_state;
  LogParser_t parser;
  char kar;

  double dpdt = 0.0;
  double user_pnow = 0.0;
  double user_piston = 0.0;

  double timeout = 0.3; // seconds
  struct timeval tvtmo;
  struct timeval tvnow;
  struct timeval tvdac;
  struct timezone tz;
  double elapsed_secs = 0.0;

  int got_piston = 0;
  int got_pnow = 0;
  int pid = getpid();
  int option_index = 0;
  int c;

  while ((c = getopt_long(argc, argv, "p:b:r?", longopts, &option_index)) != -1)
  {
      switch(c)
      {
          case 'b':
              baud = atol(optarg);
              fprintf(stderr,"Float console baud=%ld\n",baud);
              break;
          case 'p':
              user_pnow = atof(optarg);
              got_pnow = 1;
              fprintf(stderr,"Initial pressure=%g\n",user_pnow);
              break;
          case 0:
              user_piston = atof(optarg);
              got_piston = 1;
              fprintf(stderr,"Initial piston=%g\n",user_piston);
              break;
          case 'r':
              fprintf(stderr, "%s (%s)\n", VERSION, GIT_REV);
              exit(0);
          case '?':
              usage();
              exit(0);
              break;
          default:
              usage();
              exit(2);
              break;
      }
  }
  argc -= optind;
  argv += optind;

  if(argc != 4)
  {
      usage();
      exit(2);
  }

  // Parse required arguments
  serno = atol(*argv++);
  ptsfile = *argv++;
  ttyema = *argv++;
  ttydac = *argv++;

  if (signal(SIGHUP, sighup_handler) == SIG_ERR)
  {
      perror("signal handler setup");
      exit(1);
  }

  fprintf(stderr,"pid: %d\n",pid);

  pr_cal = lookup_cal(serno);
  if (pr_cal == NULL)
  {
    fprintf (stderr, "error: unknown CTD serno=%ld\n", serno);
    exit(1);
  }

  read_pts();

  if ((fdema = opentty (ttyema, baud)) == -1)
  {
    fprintf (stderr, "error: cannot open ttyema=\"%s\"\n", ttyema);
    exit(1);
  }
  if ((fddac = opentty (ttydac, 9600)) == -1)
  {
    fprintf (stderr, "error: cannot open ttydac=\"%s\"\n", ttydac);
    exit(1);
  }

  gettimeofday(&tvdac, &tz);

  Tnow = Tprof[0];
  Snow = Sprof[0];

  // Initialize the system state structure
  state_init(&sys_state, Pprof[0]);
  sys_state.got_pnow = got_pnow;
  sys_state.got_piston = got_piston;
  sys_state.pnow = user_pnow;
  sys_state.piston_now = user_piston;
  if(sys_state.got_pnow == 1)
      write_dac(fddac, sys_state.pnow, pr_cal);

  // Initialize the parser
  if(parser_init(&parser, 512) != STATUS_OK)
  {
      perror("parser_init failed");
      exit(1);
  }

  printf("Starting simdac version %s\n", VERSION);

  while (1)                     // main loop
  {
    ParserState pstate;
    FD_ZERO (&readfds);
    FD_SET (fdema, &readfds);

    nfds = 0;
    if (fdema >= 0 && fdema > nfds) nfds = fdema;
    nfds += 1;

    tvtmo.tv_sec = 0;
    tvtmo.tv_usec = timeout * 1e6 + 10000;  // microseconds

    if ((selret = select (nfds, &readfds, NULL, NULL, &tvtmo)) < 0)
    {
        perror("select failed");
        continue;
    }

    if (selret > 0)             // something's available
    {

      if (FD_ISSET (fdema, &readfds))
      {
        if (read (fdema, &kar, 1) != 1)
        {
            perror("read failed");
            exit (1);
        }

        // Use the character to update the system state
        pstate = state_next(&sys_state, &parser, (int)kar);
#ifdef DEBUG_PARSER
        fprintf(stderr, "char=%c state=%d\n", kar, pstate);
#endif
        switch (pstate)
        {
            case DONE:
            case PARSE_ERROR:
                parser_reset(&parser);
                break;
            default:
                break;
        }

      }
    }

    // get here with each character or a timeout

    if(sys_state.got_piston==0 || sys_state.got_pnow==0)
      continue;

    gettimeofday(&tvnow, &tz);

    elapsed_secs = (tvnow.tv_sec - tvdac.tv_sec) +
                   (tvnow.tv_usec - tvdac.tv_usec) * 1.0e-6;

    if (elapsed_secs > timeout)
    {
      int dac;
      double piston;
      double piston_diff;

      // Update the piston position
      piston = state_adv_piston(&sys_state, &tvnow);

      if (piston < 9) piston = 9;
      if (piston > 227) piston = 227;

      // Update the pressure in response to the piston change
      piston_diff = piston - piston_neutral(&sys_state);
      if (piston_diff >= 0.0)
        dpdt = -sqrt(piston_diff) * 0.02; // up
      else
        dpdt = sqrt(-piston_diff) * 0.02; // down

      if(!sys_state.valve_closed)
          sys_state.pnow += dpdt * elapsed_secs;

      // don't allow the float to be much above the surface
      if(sys_state.pnow < -0.3)
        sys_state.pnow = -0.3;

      // don't allow the float to be much above the surface
      if(sys_state.pnow < Pprof[0])
        sys_state.pnow = Pprof[0];

      // assume bottom of ocean at last Pprof
      if(sys_state.pnow > Pprof[nprof-1])
        sys_state.pnow = Pprof[nprof-1];

      // Output scaled pressure value on DAC and return the scaled value
      dac = write_dac(fddac, sys_state.pnow, pr_cal);
      gettimeofday(&tvdac, &tz);

      printf("\r%s PC=%6.1f dt=%6.3f dpdt=%6.3f P=%7.2f T=%6.3f S=%6.3f dac=%04X\033[K",
             isonow(), piston, elapsed_secs, dpdt, sys_state.pnow, Tnow, Snow, dac);
      fflush(stdout);

    }
  } // while (1)                     // main loop
  exit (0);
}
