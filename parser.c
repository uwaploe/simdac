//
// Simple parser for Apf-11 console log output.
//
#include "parser.h"

#define BUF_INC         256

int cb_init(CharBuf_t *p, size_t init_size)
{
    if((p->buf = malloc(init_size)) == NULL)
        return ENOMEM;
    p->bufsize = init_size;
    p->ibuf = 0;

    return STATUS_OK;
}

void cb_free(CharBuf_t *p)
{
    free(p->buf);
    p->buf = 0;
    p->ibuf = 0;
    p->bufsize = 0;
}

int cb_add_char(CharBuf_t *p, int c)
{
    p->buf[p->ibuf++] = c;
    if(p->ibuf >= (p->bufsize - 1))
    {
        p->bufsize += BUF_INC;
        p->buf = realloc(p->buf, p->bufsize);
        if(p->buf == NULL)
            return ENOMEM;
    }
    // Keep buffer 0 terminated
    p->buf[p->ibuf] = 0;

    return STATUS_OK;
}

void cb_reset(CharBuf_t *p)
{
    p->ibuf = 0;
    p->buf[0] = 0;
}

int parser_init(LogParser_t *p, size_t bufsize)
{
    int         status;

    p->state = START;
    p->hold = INVALID;
    if((status = cb_init(&(p->buf), bufsize)) != STATUS_OK)
        return status;
    if((status = cb_init(&(p->token), 32)) != STATUS_OK)
        return status;
    return cb_init(&(p->func_name), 64);
}

void parser_reset(LogParser_t *p)
{
    p->state = START;
    p->hold = INVALID;
    cb_reset(&(p->buf));
    cb_reset(&(p->token));
    cb_reset(&(p->func_name));
}

ParserState parser_add_char(LogParser_t *p, int c)
{
    int         status;

    if(p->state == DONE)
        return DONE;

    if((status = cb_add_char(&(p->buf), c)) != STATUS_OK)
        p->state = PARSE_ERROR;

    if(c == '\r' || c == '\n')
        p->state = DONE;

    switch (p->state)
    {
        case START:
            if(c == '(')
                p->state = RD_TIMESTAMP;
            break;
        case RD_TIMESTAMP:
            if(c == ',')
                p->state = RD_CLOCK;
            break;
        case RD_CLOCK:
            if(c == ')')
            {
                p->hold = RD_FUNCNAME;
                p->state = SKIP_SPACES;
                cb_reset(&(p->func_name));
            }
            break;
        case SKIP_SPACES:
            if(c != ' ')
            {
                p->state = p->hold;
                switch(p->hold)
                {
                    case RD_FUNCNAME:
                        cb_add_char(&(p->func_name), c);
                        break;
                    case RD_TEXT:
                        cb_add_char(&(p->token), c);
                        break;
                    default:
                        break;
                }
            }
            break;
        case RD_FUNCNAME:
            cb_add_char(&(p->func_name), c);
            if(c == ')')
            {
                p->hold = RD_TEXT;
                p->state = SKIP_SPACES;
                cb_reset(&(p->token));
            }
            break;
        case RD_TEXT:
            if(c == ' ')
                p->state = GOT_TOKEN;
            else
                cb_add_char(&(p->token), c);
            break;
        case GOT_TOKEN:
            if(c != ' ')
            {
                cb_reset(&(p->token));
                cb_add_char(&(p->token), c);
                p->state = RD_TEXT;
            }
            break;
        case DONE:
            break;
        case PARSE_ERROR:
            break;
        case INVALID:
            p->state = PARSE_ERROR;
            break;
    }

    return p->state;
}

const char *parser_funcname(LogParser_t *p)
{
    return p->func_name.buf;
}

const char *parser_token(LogParser_t *p)
{
    return p->token.buf;
}

const char *parser_line(LogParser_t *p)
{
    return p->buf.buf;
}
