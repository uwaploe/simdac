# include <math.h>
# include "seawater.h"

double
sw_dens0 (double S, double T)
{
// converted from Matlab to C -- John H. Dunlap, APL-UW, May 12, 2005

// SW_DENS0   Denisty of sea water at atmospheric pressure
//=========================================================================
// SW_DENS0  $Revision: 1.3 $  $Date: 1994/10/10 04:54:09 $
//           Copyright (C) CSIRO, Phil Morgan 1992
//
// USAGE:  dens0 = sw_dens0(S,T)
//
// DESCRIPTION:
//    Density of Sea Water at atmospheric pressure using
//    UNESCO 1983 (EOS 1980) polynomial.
//
// INPUT:  (all must have same dimensions)
//   S = salinity    [psu      (PSS-78)]
//   T = temperature [degree C (ITS-90)]
//
// OUTPUT:
//   dens0 = density  [kg/m^3] of salt water with properties S,T,
//           P=0 (0 db gauge pressure)
//
// AUTHOR:  Phil Morgan 92-11-05  (morgan@ml.csiro.au)
//
// DISCLAIMER:
//   This software is provided "as is" without warranty of any kind.
//   See the file sw_copy.m for conditions of use and licence.
//
// REFERENCES:
//     Unesco 1983. Algorithms for computation of fundamental properties of
//     seawater, 1983. _Unesco Tech. Pap. in Mar. Sci._, No. 44, 53 pp.
//
//     Millero, F.J. and  Poisson, A.
//     International one-atmosphere equation of state of seawater.
//     Deep-Sea Res. 1981. Vol28A(6) pp625-629.
//=========================================================================

// CALLER: general purpose, sw_dens.m
// CALLEE: sw_smow.m

//----------------------
// DEFINE CONSTANTS
//----------------------
//     UNESCO 1983 eqn(13) p17.

  double b0 = 8.24493e-1;
  double b1 = -4.0899e-3;
  double b2 = 7.6438e-5;
  double b3 = -8.2467e-7;
  double b4 = 5.3875e-9;

  double c0 = -5.72466e-3;
  double c1 = +1.0227e-4;
  double c2 = -1.6546e-6;

  double d0 = 4.8314e-4;

  double T90 = T;
  double dens;

  T *= 1.00024;                 // convert from T90 to T68

  dens = sw_smow (T90) + (b0 + (b1 + (b2 + (b3 + b4 * T) * T) * T) * T) * S
    + (c0 + (c1 + c2 * T) * T) * S * sqrt (S) + d0 * S * S;

  return dens;
//--------------------------------------------------------------------
}
