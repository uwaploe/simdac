# include "seawater.h"

double
sw_adtg (double S, double T, double P)
{
// ported from Matlab to C -- John H. Dunlap, APL-UW, May 12, 2005

// SW_ADTG    Adiabatic temperature gradient
//===========================================================================
// SW_ADTG   $Revision: 1.4 $  $Date: 1994/10/10 04:16:37 $
//           Copyright (C) CSIRO, Phil Morgan  1992.
//
// adtg = sw_adtg(S,T,P)
//
// DESCRIPTION:
//    Calculates adiabatic temperature gradient as per UNESCO 1983 routines.
//
// INPUT:  (all must have same dimensions)
//   S = salinity    [psu      (PSS-78) ]
//   T = temperature [degree C (ITS-90)]
//   P = pressure    [db]
//
// OUTPUT:
//   ADTG = adiabatic temperature gradient [degree_C/db]
//
// AUTHOR:  Phil Morgan 92-04-03  (morgan@ml.csiro.au)
//
// DISCLAIMER:
//   This software is provided "as is" without warranty of any kind.
//   See the file sw_copy.m for conditions of use and licence.
//
// REFERENCES:
//    Fofonoff, P. and Millard, R.C. Jr
//    Unesco 1983. Algorithms for computation of fundamental properties of
//    seawater. Unesco Tech. Pap. in Mar. Sci., No. 44, 53 pp.  Eqn.(31) p.39
//
//    Bryden, H. 1973.
//    "New Polynomials for thermal expansion, adiabatic temperature gradient
//    and potential temperature of sea water."
//    DEEP-SEA RES., 1973, Vol20,401-408.
//=========================================================================

//-------------
// BEGIN
//-------------
  double a0 = 3.5803E-5;
  double a1 = +8.5258E-6;
  double a2 = -6.836E-8;
  double a3 = 6.6228E-10;

  double b0 = +1.8932E-6;
  double b1 = -4.2393E-8;

  double c0 = +1.8741E-8;
  double c1 = -6.7795E-10;
  double c2 = +8.733E-12;
  double c3 = -5.4481E-14;

  double d0 = -1.1351E-10;
  double d1 = 2.7759E-12;

  double e0 = -4.6206E-13;
  double e1 = +1.8676E-14;
  double e2 = -2.1687E-16;

  double ADTG;

  T *= 1.00024;                 // convert from T90 to T68

  ADTG = a0 + (a1 + (a2 + a3 * T) * T) * T
    + (b0 + b1 * T) * (S - 35)
    + ((c0 + (c1 + (c2 + c3 * T) * T) * T) + (d0 + d1 * T) * (S - 35)) * P
    + (e0 + (e1 + e2 * T) * T) * P * P;

  return ADTG;
//==========================================================================
}
